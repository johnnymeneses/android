package com.johnny.motivation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.johnny.motivation.R
import com.johnny.motivation.infra.MotivationConstants
import com.johnny.motivation.infra.SecurityPreferences
import com.johnny.motivation.repository.Mock
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var mSecurityPreferences: SecurityPreferences
    private var mPhraseFilter: Int = MotivationConstants.PHRASEFILTER.ALL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mSecurityPreferences = SecurityPreferences(this)

        text_name.text =
            "Olá, ${mSecurityPreferences.getString(MotivationConstants.KEY.PERSON_NAME)}!"

        supportActionBar!!.hide()

        handlePhrase()
        image_emoji.setOnClickListener (this)
        image_all.setOnClickListener (this)
        image_sun.setOnClickListener (this)
        btn_novaFrase.setOnClickListener(this)


    }

    override fun onClick(view: View) {
        val id = view.id

        val listFiler = listOf(R.id.image_all,R.id.image_emoji,R.id.image_sun)

       if(id ==R.id.btn_novaFrase){
           handlePhrase()
       }else if (id in listFiler){
           handleFilter(id)
       }

    }

    private fun handleFilter(id: Int) {
        when (id) {
            R.id.image_all -> {
                ligaImagens()
                mPhraseFilter = MotivationConstants.PHRASEFILTER.ALL
            }
            R.id.image_emoji -> {
               ligaEmoji()
                mPhraseFilter = MotivationConstants.PHRASEFILTER.EMOJI
            }
            R.id.image_sun -> {
                ligaSun()
                mPhraseFilter = MotivationConstants.PHRASEFILTER.SUN
            }

        }
    }

    private fun handlePhrase() {
        text_frase.text = Mock().getPhrase(mPhraseFilter)

    }


    private fun ligaImagens(){
        image_all.setColorFilter(resources.getColor(R.color.white))
        image_emoji.setColorFilter(resources.getColor(R.color.white))
        image_sun.setColorFilter(resources.getColor(R.color.white))
    }
    private fun ligaSun(){
        image_all.setColorFilter(resources.getColor(R.color.colorAccent))
        image_emoji.setColorFilter(resources.getColor(R.color.colorAccent))
        image_sun.setColorFilter(resources.getColor(R.color.white))
    }

    private fun ligaEmoji(){
        image_all.setColorFilter(resources.getColor(R.color.colorAccent))
        image_emoji.setColorFilter(resources.getColor(R.color.white))
        image_sun.setColorFilter(resources.getColor(R.color.colorAccent))
    }

}