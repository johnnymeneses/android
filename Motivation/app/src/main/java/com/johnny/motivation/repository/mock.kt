package com.johnny.motivation.repository


import android.icu.lang.UCharacterCategory
import com.johnny.motivation.infra.MotivationConstants
import kotlin.random.Random

data class Phrase(val description: String, val category: Int)


class Mock {

    private val ALL = MotivationConstants.PHRASEFILTER.ALL
    private val EMOJI = MotivationConstants.PHRASEFILTER.EMOJI
    private val SUN = MotivationConstants.PHRASEFILTER.SUN

    private val mListPhrase: List<Phrase> = listOf(

        Phrase("Não sabendo que era impossível, foi lá e soube", ALL),
        Phrase("Se você não pode fazer tudo, nem comece", EMOJI),
        Phrase("Você não pode mudar o seu futuro, mas irá estragar o seu passado", ALL),
        Phrase("Nunca duvide do que você é incapaz", EMOJI),
        Phrase("Trabalhe enquanto eles herdam", ALL),
        Phrase("Lute como nunca, perca como sempre", ALL),
        Phrase("Nunca foi azar, sempre foi incompetência", ALL),
        Phrase("Não é porque deu errado até agora, que vai dar certo daqui pra frente", ALL),
        Phrase("Se você pode sonhar, você pode desistir", ALL),
        Phrase("É sobre isso e tá tudo uma bosta", ALL),
        Phrase("No começo tava ruim, agora parece o começo", ALL),
        Phrase("Você ainda não chegou lá, mas olha o quanto você já se fodeu", ALL),
        Phrase("Se você trabalha para pagar as contas, você é pobre", ALL),
        Phrase("Não pare até se humilhar", ALL),
        Phrase("É só uma fase ruim, vai piora,r", ALL),
        Phrase("Tudo dando errado conforme esperado", ALL),
        Phrase("Parabéns, ficou uma bosta", ALL),
        Phrase("Calma, você ainda tem muito a perder", ALL),
        Phrase("A vida é feita de obstáculos e você tropeça em todos", ALL),
        Phrase("Persista, insista e desista", ALL)
    )

    fun getPhrase(categoryID: Int) : String{

        val filtered = mListPhrase.filter { (it.category == categoryID || categoryID==ALL )} //?

        val rand = Random.nextInt(mListPhrase.size)

        return filtered[rand].description
    }

}