package com.johnny.motivation.infra

class MotivationConstants private constructor() { //construtor privado para que a classe não possa ser inicializada

    object KEY {
        val PERSON_NAME = "name"
    }


    object PHRASEFILTER {
        val ALL = 1
        val SUN = 2
        val EMOJI = 3
    }


}