package com.johnny.jpix

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_tela_login.*
import org.json.JSONObject
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class telaLogin : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tela_login)

        supportActionBar!!.hide()

        //Declarar o botão direto
        btn_entrar.setOnClickListener(this)


    }

    override fun onClick(view: View) {
        val id = view.id
        if (id == R.id.btn_entrar) {
            autentica()
        }

    }

    //Melhorar
    private fun autentica() {
        var email = edit_email.text.toString()
        var senha = edit_senha.text.toString()

        if (email == "" && senha == "") {
//            Toast.makeText(this, "Preencha todos os campos", Toast.LENGTH_SHORT).show()
            telaPrincipal()
        } else {
            telaPrincipal()
        }

    }


    private fun telaPrincipal() {
        val intent = Intent(this, telaPrincipal::class.java)
        startActivity(intent)
        finish()
    }

}
