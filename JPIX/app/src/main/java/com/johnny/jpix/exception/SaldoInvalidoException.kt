package com.johnny.jpix.exception

import java.lang.Exception

class SaldoInvalidoException(
    message: String = "Saldo Insuficiente") : Exception (message)
