package com.johnny.jpix.exception

import java.lang.Exception

class ValorInvalidoException(
    message: String = "Valor Inválido") : Exception (message)
