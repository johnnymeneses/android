package com.johnny.jpix.logical

import com.johnny.jpix.exception.SaldoInvalidoException
import com.johnny.jpix.exception.ValorInvalidoException


class Conta(var cliente: Cliente, val agencia: Int, val conta: Int) {

    var saldo: Double = 500.0
        private set


    fun imprimeDadosContas() {
        println("Nome: ${cliente.nomeCliente}")
        println("Agencia: $conta")
        println("Conta: $agencia")
        println("Saldo $saldo")
    }

    fun imprimeSaldo() {
        println("Saldo do cliente : R$ $saldo")
    } //100%

    fun depositaValor(valor: Double) {

        when {
            validaValor(valor) -> throw ValorInvalidoException()
            else -> saldo += valor
        }
    } //100%

    fun saqueValor(valor: Double) {

        when {
            validaValor(valor) -> throw ValorInvalidoException()
            validaSaldo(valor) -> throw SaldoInvalidoException()
            else -> saldo -= valor
        }


    } //100%

    fun pixValor(chavePix: Conta, valor: Double) {
        this.saqueValor(valor)
        chavePix.depositaValor(valor)
    }


    //Valor menor igual que zero: Invalido
    fun validaValor(valor: Double): Boolean {
        return valor <= 0
    } //100%

    //Valor menor que o saldo: Invalido
    fun validaSaldo(valor: Double): Boolean {
        return valor > saldo
    }


}



