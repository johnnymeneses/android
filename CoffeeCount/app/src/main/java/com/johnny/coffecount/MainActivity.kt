package com.johnny.coffecount

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var contador: Int = 0
    var contadorAgua: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.hide()

        iniciaQtde()

        //Adiciona Bebidas
        img_coffee.setOnClickListener() { somaBebida("c") }

        img_agua.setOnClickListener() { somaBebida("a") }


        //Zera Bebidas
        img_agua.setOnLongClickListener() { zeraBebida("a") }

        img_coffee.setOnLongClickListener() { zeraBebida("c") }

    }

    fun iniciaQtde() {
        qtde_agua.text = "Nenhum copo de água"
        qtde_cafee.text = "Nenhum copo de café"
    }

    fun somaBebida(opcao: String) {

        //Cafe
        if (opcao == "c") {
            contador += 1
            when {
                contador == 1 -> "$contador Cafe".also { qtde_cafee.text = it }
                contador > 1 -> "$contador Cafés".also { qtde_cafee.text = it }
            }
        }

        //Agua
        if (opcao == "a") {
            contadorAgua += 1
            when {
                contadorAgua == 1 -> "$contadorAgua copo de Agua".also { qtde_agua.text = it }
                contadorAgua > 1 -> "$contadorAgua copos de Agua".also { qtde_agua.text = it }
            }
        }


    }

    fun zeraBebida(opcao: String): Boolean {

        //Cafe
        if (opcao == "c") {
            contador = 0
            qtde_cafee.text = "Nenhum copo de café"
        }

        //Agua
        if (opcao == "a") {
            contadorAgua = 0
            qtde_agua.text = "Nenhum copo de água"
        }
        return true
    }


}